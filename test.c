#include <cairo.h>
#include <gtk/gtk.h>

struct {
  cairo_surface_t *image_nube1;  
  cairo_surface_t *image_nube2; 
  cairo_surface_t *image_globo;
} glob;


static void do_drawing(cairo_t *);

static gboolean on_draw_event(GtkWidget *widget, cairo_t *cr, 
    gpointer user_data)
{      
  do_drawing(cr);

  return FALSE;
}

static void do_drawing(cairo_t *cr)
{
  cairo_set_source_surface(cr, glob.image_nube1, 10, 10);
  cairo_paint(cr);
  cairo_set_source_surface(cr, glob.image_nube2, 200, 50);
  cairo_paint(cr);  
  cairo_set_source_surface(cr, glob.image_globo, 150, 150);
  cairo_paint(cr);  
}


int main(int argc, char *argv[])
{
  GtkWidget *ventana;
  GtkWidget *area_dibujo;
  
  glob.image_nube1 = cairo_image_surface_create_from_png("nube.png");
  glob.image_nube2 = cairo_image_surface_create_from_png("nube.png");
  glob.image_globo = cairo_image_surface_create_from_png("globo.png");

  gtk_init(&argc, &argv);

  ventana = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  area_dibujo = gtk_drawing_area_new();
  gtk_container_add(GTK_CONTAINER (ventana), area_dibujo);

  g_signal_connect(G_OBJECT(area_dibujo), "draw", 
      G_CALLBACK(on_draw_event), NULL); 
  g_signal_connect(ventana, "destroy",
      G_CALLBACK (gtk_main_quit), NULL);

  gtk_window_set_position(GTK_WINDOW(ventana), GTK_WIN_POS_CENTER);
  gtk_window_set_default_size(GTK_WINDOW(ventana), 640, 480); 
  gtk_window_set_title(GTK_WINDOW(ventana), "Nubes");

  gtk_widget_show_all(ventana);

  gtk_main();

  cairo_surface_destroy(glob.image_nube1);
  cairo_surface_destroy(glob.image_nube2);
  cairo_surface_destroy(glob.image_globo);

  return 0;
} 
