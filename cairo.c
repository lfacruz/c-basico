#include <gtk/gtk.h>
#include <math.h>
static cairo_surface_t *surface = NULL;
static int surface_width;
static int surface_height;
static void drawing_area_draw_cb (GtkWidget *, cairo_t *, void *);
int main( int argc, char *argv[] )
{
   /* Inicializa los widgets que se van a utilizar (inicialmente la ventana)*/
      GtkWidget *ventana;

   /* Arranca GTK+ */
      gtk_init (&argc, &argv);

   /* Define los tipos de widget y los configura*/
   // creamos la ventana
      ventana = gtk_window_new(GTK_WINDOW_TOPLEVEL);

   // ponemos título a la ventana
      gtk_window_set_title(GTK_WINDOW(ventana), "Hola Mundo 1");
      
      GtkWidget *drawing_area = gtk_drawing_area_new ();
      gtk_container_add (GTK_CONTAINER (ventana), drawing_area);

   // mostramos la ventana
      gtk_widget_show (ventana);

   /* Arranca el programa*/
      gtk_main ();

   /* Finaliza el programa*/
      return 0;
} 

static void
drawing_area_draw_cb (GtkWidget *widget, cairo_t *context, void *ptr)
{
	/* Copy the contents of the surface to the current context */
	if (surface != (cairo_surface_t *)NULL)
	{  
	  double xc = 128.0;
      double yc = 128.0;
      double radius = 100.0;
      double angle1 = 45.0  * (M_PI/180.0);  /* angles are specified */
      double angle2 = 180.0 * (M_PI/180.0);  /* in radians           */

      cairo_set_line_width (context, 10.0);
      cairo_arc (context, xc, yc, radius, angle1, angle2);
      cairo_stroke (context);

      /* draw helping lines */
      cairo_set_source_rgba (context, 1, 0.2, 0.2, 0.6);
      cairo_set_line_width (context, 6.0);

      cairo_arc (context, xc, yc, 10.0, 0, 2*M_PI);
      cairo_fill (context);

      cairo_arc (context, xc, yc, radius, angle1, angle1);
      cairo_line_to (context, xc, yc);
      cairo_arc (context, xc, yc, radius, angle2, angle2);
      cairo_line_to (context, xc, yc);
      cairo_stroke (context);
	  //cairo_set_source_surface (context, surface, 0, 0);
      //cairo_paint (context);
	}
} 
